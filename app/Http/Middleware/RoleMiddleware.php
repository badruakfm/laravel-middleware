<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->role;
        $nameruoute = $request->route()->getName();
        
        switch($role){
            case 'Admin':
                if($nameruoute == 'route-1'){
                    abort(403);
                }
                break;
            case 'Guest':
                if($nameruoute != 'route-3'){
                    abort(403);
                }
                break;
        }


        return $next($request);
    }
}
